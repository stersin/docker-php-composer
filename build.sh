#!/bin/sh

DOCKER_TAG=`cat ./Dockertag`

sudo docker build $* -t=$DOCKER_TAG .

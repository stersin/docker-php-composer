FROM ubuntu:14.04
MAINTAINER Sebastien TERSIN, stersin.dev@gmail.com

ENV DEBIAN_FRONTEND noninteractive

# Intall of prerequisite
RUN apt-get update && apt-get install -y git curl php5-cli php5-curl

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin

WORKDIR /src

ENTRYPOINT ["php","/usr/local/bin/composer.phar"]

CMD ["install"]

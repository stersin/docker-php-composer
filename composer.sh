#!/bin/sh

LOCALPATH=`pwd`
USER=`whoami`

sudo docker run --rm -it -v ~/.composer:/root/.composer -v $LOCALPATH:/src stersin/php-composer $*

if [ -d $LOCALPATH/vendor ]
then
	sudo chown -R $USER:$USER $LOCALPATH/vendor
fi
